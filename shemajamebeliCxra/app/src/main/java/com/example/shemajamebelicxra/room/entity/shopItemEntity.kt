package com.example.shemajamebelicxra.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="shop")
data class shopItemEntity(
    @PrimaryKey(autoGenerate = true) val uid: Int=0,
    @ColumnInfo(name = "cover") val cover: String?,
    @ColumnInfo(name = "liked") val liked: Boolean?,
    @ColumnInfo(name = "price") val price: String?,
    @ColumnInfo(name = "title") val title: String?
)
{
    constructor(cover: String, liked: Boolean,price: String,title: String,) : this(0, cover, liked,price,title)
}