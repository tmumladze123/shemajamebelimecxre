package com.example.shemajamebelicxra.network


import com.example.shemajamebelicxra.model.shopItem
import retrofit2.Response
import retrofit2.http.GET


interface shopApi {
    @GET("v3/05d71804-4628-4269-ac03-f86e9960a0bb")
    suspend fun getItems() : Response<ArrayList<shopItem>>
}