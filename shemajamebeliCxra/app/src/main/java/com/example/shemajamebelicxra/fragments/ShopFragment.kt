package com.example.shemajamebelicxra.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log.d
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shemajamebelicxra.BaseFragment
import com.example.shemajamebelicxra.ViewModel.shopViewModel
import com.example.shemajamebelicxra.adapter.shopAdapter
import com.example.shemajamebelicxra.databinding.FragmentShopBinding
import com.example.shemajamebelicxra.model.shopItem
import com.example.shemajamebelicxra.network.manageResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ShopFragment : BaseFragment<FragmentShopBinding>(FragmentShopBinding::inflate) {
    private val newsModel: shopViewModel by viewModels()
    var adapter=shopAdapter()
    override fun start() {
        initAdapter()
        if(isOnline()) {
            newsModel.connect()
            getResult()
        }
        else {
           observers()
        }
    }
    fun initAdapter() {
     binding.rvShop.adapter=adapter
     binding.rvShop.layoutManager=GridLayoutManager(context, 2)
    }
    fun getResult() {
        newsModel.newsResource.observe(viewLifecycleOwner,{
            when(it)
            {
                is manageResponse.Success -> { adapter.setData(it.data!!)}

                is manageResponse.Error -> {
                    it.message?.let { it1 -> error(it1) }
                }
            }
        })
    }

    fun observers() {
        viewLifecycleOwner.lifecycleScope.launch {
            newsModel.roomFlow?.collect {
                 var arr=ArrayList<shopItem>()
                for(element in it)
                    arr.add(shopItem(element.cover,element.liked,element.price,element.title))
                adapter.setData(arr)
            }
        }
    }


    fun isOnline() :Boolean {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var activeNetworkInfo: NetworkInfo? = null
        activeNetworkInfo = cm.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
    }
}