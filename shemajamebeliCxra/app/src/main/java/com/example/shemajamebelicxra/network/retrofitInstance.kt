package com.example.shemajamebelicxra.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object retrofitInstance {

    private val retrofit by lazy {
        Retrofit.Builder().baseUrl("https://run.mocky.io/").addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val shop: shopApi by lazy {
        retrofit.create(shopApi::class.java)
    }

}