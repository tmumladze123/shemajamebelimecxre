package com.example.shemajamebelicxra.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebelicxra.databinding.ShopitemBinding
import com.example.shemajamebelicxra.extensions.setPhoto
import com.example.shemajamebelicxra.model.shopItem

class shopAdapter( ) : RecyclerView.Adapter<shopAdapter.ViewHolder>() {
    var shopItems= mutableListOf<shopItem>()
    inner class ViewHolder(val binding: ShopitemBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var news: shopItem
        fun bind()
        {  news= shopItems[adapterPosition]
           binding.apply {
               tvName.text=news.title
               tvPrice.text=news.price
               ivShopItem.setPhoto(news.cover)
           }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): shopAdapter.ViewHolder =
        ViewHolder(ShopitemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: shopAdapter.ViewHolder, position: Int) {
        holder.bind()
    }
    override fun getItemCount(): Int =shopItems.size
    fun setData(shopItems: ArrayList<shopItem>)
    {
        this.shopItems.clear()
        this.shopItems.addAll(shopItems)
        notifyDataSetChanged()
    }
}