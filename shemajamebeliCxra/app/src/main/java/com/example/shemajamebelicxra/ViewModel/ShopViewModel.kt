package com.example.shemajamebelicxra.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shemajamebelicxra.model.shopItem
import com.example.shemajamebelicxra.network.manageResponse
import com.example.shemajamebelicxra.network.retrofitInstance
import com.example.shemajamebelicxra.room.Database.shopDataBase
import com.example.shemajamebelicxra.room.entity.shopItemEntity
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class shopViewModel : ViewModel() {
    private var _newsResource = MutableLiveData<manageResponse<ArrayList<shopItem>>>()
    val newsResource: LiveData<manageResponse<ArrayList<shopItem>>>
        get() = _newsResource

    var roomFlow: Flow<List<shopItemEntity>>? = shopDataBase.db.shoppingDao().getAll()

    fun connect() {
        viewModelScope.launch {
            withContext(IO) {
              getShop()
            }
        }
    }

    private suspend fun getShop() {
        try {
            var result = retrofitInstance.shop.getItems()
            var body = result.body()
            if (result.isSuccessful && body != null)
            {
                _newsResource.postValue(manageResponse.Success(body))
                for(element in body!!)
                {
                    shopDataBase.db.shoppingDao().insertAll(
                        shopItemEntity( element.cover!!, element.liked!!, element.price!!, element.title!!)
                    )
                }
            }
            else
                _newsResource.postValue(manageResponse.Error("error"))
        } catch (e: Exception) {
            _newsResource.postValue(manageResponse.Error("error"))
            println("aqane daerorda simon")
        }
    }

     fun del()
    {
        shopDataBase.db.shoppingDao().delAll()
    }
}
