package com.example.shemajamebelicxra.model


import com.google.gson.annotations.SerializedName

data class shopItem(
    @SerializedName("cover")
    val cover: String?,
    @SerializedName("liked")
    val liked: Boolean?,
    @SerializedName("price")
    val price: String?,
    @SerializedName("title")
    val title: String?
)