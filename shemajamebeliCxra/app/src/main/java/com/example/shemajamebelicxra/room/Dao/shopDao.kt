package com.example.shemajamebelicxra.room.Dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.shemajamebelicxra.room.entity.shopItemEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface shopDao {
    @Query("SELECT * FROM shop")
    fun getAll(): Flow<List<shopItemEntity>>

    @Insert
    fun insertAll(vararg items: shopItemEntity)

    @Delete
    fun delete(items: shopItemEntity)

    @Query("DELETE FROM shop")
    fun delAll()
}