package com.example.shemajamebelicxra.network



sealed class manageResponse<T>(
    val data: T? = null,
    val message: String? = null
) {
    class Success<T>(data: T) : manageResponse<T>(data)

    class Error<T>(message: String?, data: T? = null) : manageResponse<T>(data, message)

}