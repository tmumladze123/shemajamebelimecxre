package com.example.shemajamebelicxra.room.Database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.shemajamebelicxra.contextClass
import com.example.shemajamebelicxra.room.Dao.shopDao
import com.example.shemajamebelicxra.room.entity.shopItemEntity


@Database(entities = [shopItemEntity::class], version = 1)
abstract class shopDataBase : RoomDatabase() {
    abstract fun shoppingDao(): shopDao
    companion object {
        val db = Room.databaseBuilder(
            contextClass.context!!,
            shopDataBase::class.java, "database-name"
        ).build()
    }
}